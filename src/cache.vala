using Soup;
using Json;
using Gee;

namespace Cache {
	public const string[] Categories = {
		"Arcanes",
		"Arch-Gun",
		"Arch-Melee",
		"Archwing",
		"Enemy",
		"Fish",
		"Gear",
		"Glyphs",
		"Melee",
		"Misc",
		"Mods",
		"Node",
		"Pets",
		"Primary",
		"Quests",
		"Relics",
		"Resources",
		"Secondary",
		"SentinelWeapons",
		"Sentinels",
		"Sigils",
		"Skins",
		"Warframes"
	};

	private HashMap<string, ArrayList> name_cache;
	private ArrayList<string> names;

	public void fetch_data()
	{
		string downloadURL = "http://raw.githubusercontent.com/WFCD/warframe-items/development/data/json/";
		string separator = GLib.Path.DIR_SEPARATOR.to_string();
		string cachePath = Environment.get_user_cache_dir() + separator + Environment.get_application_name() + separator
		                   + "cache";

		message("%s", cachePath);
		var path = File.new_for_path(cachePath);

		try{
			if (path.query_exists()) {
				FileEnumerator childs = path.enumerate_children(FileAttribute.STANDARD_NAME, FileQueryInfoFlags.NONE);

				string filename = null;
				while ((filename = childs.next_file().get_name()) != null) {
					filename = cachePath + separator + filename;
					File.new_for_path(filename).delete();
				}
				path.delete();
			}
			path.make_directory_with_parents();

			// file.create(FileCreateFlags.REPLACE_DESTINATION);

			Session session = new Session();

			foreach (string categaory in Categories) {
				Message request_message = new Message("GET", downloadURL + categaory + ".json");
				message("Fetching  %s %s.json", downloadURL, categaory);

				InputStream inputStream = session.send(request_message);
				DataInputStream dataStream = new DataInputStream(inputStream);

				File dataFile = File.new_for_path(cachePath + separator + categaory + ".json");
				var ioStream = dataFile.create_readwrite(FileCreateFlags.PRIVATE);
				DataOutputStream outputStream = new DataOutputStream(ioStream.output_stream);

				message("Writing %s", dataFile.get_path());

				string? line;
				while ((line = dataStream.read_line()) != null) {
					//print(line);
					//print("\n");
					outputStream.write((line + "\n").data);
				}
				outputStream.close();
				inputStream.close();
				dataStream.close();
			}
		}catch (Error e) {
			critical("Error: %s\n", e.message);
		}
	}

	public Parser? parse_json(string category){
		var parser = new Json.Parser();
		string separator = GLib.Path.DIR_SEPARATOR.to_string();
		string cachePath = Environment.get_user_cache_dir() + separator + Environment.get_application_name() + separator
		                   + "cache";
		string cacheFile = cachePath + separator + category + ".json";
		message("Attempting to read file %s", cacheFile);
		File dataFile = File.new_for_path(cacheFile);
		if (dataFile.query_exists()) {
			try{
				FileIOStream inputStream = dataFile.open_readwrite();
				parser.load_from_stream(inputStream.get_input_stream());
				return parser;
			} catch (Error e) {
				critical("Error: %s\n", e.message);
			}
		}else{
			critical("Cache File not found! You have to generate the caches first!");
			fetch_data();
		}
		return null;
	}

	public void init_name_cache(){
		if (name_cache == null) {
			name_cache = new HashMap <string, ArrayList> ();
			names = new ArrayList <string> ();
			foreach (var category in Categories) {
				//var data = read_cache(category);
				var parser = parse_json(category);
				if (parser != null) {
					var names_list = new ArrayList <string> ();
					var root = parser.get_root();
					var elements = root.get_array().get_elements();
					foreach (var element in elements) {
						var name = element.get_object().get_string_member("name");
						names_list.add(name);
						names.add(name);
					}
					name_cache.set(category, names_list);
				}
			}
		}else{
			message("name_cache is already initialized!");
		}
		//name_cache = name_map;
	}

	public ArrayList? get_item_attributes(string category, string item_name){
		message(item_name);
		var parser = parse_json(category);
		if (parser != null) {
			var root = parser.get_root();
			foreach (var element in root.get_array().get_elements()) {
				var member = element.get_object().get_string_member("name");
				if (member == item_name) {
					var attributes = new ArrayList<string>();
					foreach (var attribute in element.get_object().get_members()) {
						attributes.add(attribute);
					}
					return attributes;
				}
			}
		}
		return null;
	}

	public void store_blacklist(ArrayList<string> blacklist, string category){
		var builder = new Json.Generator();
		string separator = GLib.Path.DIR_SEPARATOR.to_string();
		string cachePath = Environment.get_user_cache_dir() + separator + Environment.get_application_name();
		string cacheFile = cachePath + separator + "blacklists" + separator + category + ".json";
		//var blacklist_node = Json.gobject_serialize(blacklist);
		var json_array = new Json.Array();
		foreach (var item in blacklist) {
			json_array.add_string_element(item);
		}
		var blacklist_node = new Json.Node(Json.NodeType.ARRAY);
		blacklist_node.set_array(json_array);
		builder.set_root(blacklist_node);
		OutputStream outputStream;
		try {
			//File.new_for_path(cachePath + separator + "blacklists" + separator).make_directory_with_parents();
			DirUtils.create_with_parents(cachePath + separator + "blacklists" + separator, 0777);
			builder.to_file(cacheFile);
		} catch (Error e) {
			critical("Error: %s\n", e.message);
		}
	}

	public ArrayList<string> load_blacklist(string category){
		var parser = new Json.Parser();
		string separator = GLib.Path.DIR_SEPARATOR.to_string();
		string cachePath = Environment.get_user_cache_dir() + separator + Environment.get_application_name();
		string cacheFile = cachePath + separator + "blacklists" + separator + category + ".json";

		try{
			parser.load_from_file(cacheFile);
			//var blacklist = Json.gobject_deserialize(typeof (ArrayList), parser.get_root()) as ArrayList<string>;
			var json_array = parser.get_root().get_array();
			var blacklist = new ArrayList<string>();
			foreach (var element in json_array.get_elements()) {
				blacklist.add(element.get_string());
			}
			return blacklist;
		} catch (Error e) {
			critical("Error: %s\n", e.message);
		}
		return new ArrayList<string>();
	}

	public ArrayList? get_category_attributes(string category){
		var parser = parse_json(category);
		if (parser != null) {
			var root = parser.get_root();
			//At this point, the first element of this list is taken as reference.
			//It is assumed that all elements of the same category share the same attributes
			//even if these wouldn't be neccessary for a certain element …
			var default_element = root.get_array().get_elements().nth_data(0);
			var attributes = new ArrayList<string>();
			foreach (var attribute in default_element.get_object().get_members()) {
				attributes.add(attribute);
			}
			return attributes;
		}
		return null;
	}

	public ArrayList<string>? get_names(){
		return names;
	}
}
