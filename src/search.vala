using Cache;
using Gee;
using Worldinfo;
using Values;

namespace Search {

	private const string[] keywords = {
		"world",
		"news",
		"events",
		"sortie",
		"syndicates",
		"fissures",
		"invasions",
		"earth",
		"cetus",
		"vallis",
		"outposts",
		"arbitration",
		"nightwave",
	};

	public string? get_type(string query){
		var name_cache = Cache.name_cache;
		foreach (var category in Cache.Categories) {
			if (!(name_cache.is_empty) && !(name_cache == null)) {
				ArrayList<string> names = name_cache.get(category);
				if (names.contains(query)) {
					message("Found query %s in category %s.", query, category);
					return category;
				}
			}
		}
		return null;
	}

	public ArrayList? get_completions(string query)
	{
		debug("Querry was %s", query);
		var item_type = "";
		var requested_item = "";
		var only_names = false;
		if (query.contains(">")) {
			item_type = get_type(query.split(">")[0]);
			requested_item = query.split(">")[0];
			only_names = false;
		}else{
			item_type = get_type(query);
			requested_item = query;
			only_names = true;
		}

		debug("Requested item '%s' of type %s; only_names=%s", requested_item, item_type, only_names.to_string());

		if (only_names) {
			/*
			   var item_names_list = new ArrayList<string>();
			   foreach (ArrayList<string> value_array in Cache.name_cache.values) {
			        foreach (var key in value_array) {
			                item_names_list.add(key);
			                //message(key);
			        }
			   }
			 */
			var compeltion = new ArrayList<string>();
			compeltion.add_all(get_names());
			compeltion.add_all_array(keywords);
			return compeltion;
		}else{
			if (item_type != "") {
				ArrayList<string> attributes = Cache.get_item_attributes(item_type, requested_item);
				var item_names_list = new ArrayList<string>();
				var blacklist = load_blacklist(item_type);
				foreach (var attribute in attributes) {
					if (!(attribute in blacklist))
						item_names_list.add(requested_item + ">" + attribute);
					debug(requested_item + ">" + attribute);
				}
				return item_names_list;
			}
		}

		return null;
	}

	public Json.Node? do_advanced_search(string item_type, string query){
		string requested_item;
		string requested_item_sub;
		if (query.contains(">")) {
			requested_item = query.split(">")[0];
			requested_item_sub = query.split(">")[1];
		}else{
			requested_item = query;
			requested_item_sub = null;
		}
		var items = Cache.parse_json(item_type).get_root().get_array();
		foreach (var item in items.get_elements()) {
			if (item.get_object().get_string_member("name").down() == requested_item.down()) {
				if ((requested_item_sub != null) &&  (item.get_object().get_member(requested_item_sub) != null)) {
					message("%s -> %s -> %s: item[%s]", item_type, requested_item, requested_item_sub, requested_item_sub);
					return item.get_object().get_member(requested_item_sub);
				}else{
					message("no valid sub-catagory given … returning raw-data");
					return item;
				}
			}else{
				continue;
			}
		}
		message("Somehow, lookup of requested entry %s in category %s failed! Giving up :(", requested_item, item_type);
		return null;
	}


	public Json.Node? do_search(string query){
		string item_type;

		var settings = new Settings("com.archyt.cephalon-6015");
		var suppress_uniqueNames = settings.get_boolean(SUPPRESS_UNIQUE_NAMES);
		var suppress_names = settings.get_boolean(SUPPRESS_NAMES);
		debug("suppressing uniqueNames? %s", suppress_uniqueNames.to_string());
		debug("suppressing names? %s", suppress_names.to_string());

		if (query in keywords) {
			return Worldinfo.get_info();
		}else {

			if (query.contains(">")) {
				item_type = get_type(query.split(">")[0]);
			}else{
				item_type = get_type(query);
			}

			Json.Node result;
			if (item_type != null) {
				var blacklist = Cache.load_blacklist(item_type);
				message("Found query \"%s\" in category %s", query, item_type);
				result = do_advanced_search(item_type, query.down());
				if (result != null) {
					if (result.get_node_type() == Json.NodeType.OBJECT) {
						foreach (var key in blacklist) {
							var result_obj = result.get_object();
							if (result_obj.get_member(key)!= null) {
								result_obj.remove_member(key);
							}
							result.set_object(result_obj);
						}
						var result_obj = result.get_object();
						if (suppress_uniqueNames)
							debug("suppressing unqueName");
						if (result_obj.get_member("uniqueName")!= null) {
							result_obj.remove_member("uniqueName");
						}
						if (suppress_names)
							if (result_obj.get_member("name")!= null) {
								result_obj.remove_member("name");
							}
						result.set_object(result_obj);
						return result;

					}
					return result;
				}
			}
		} return null;
	}
}
