using Soup;
using Json;
using Watcher;
using Gtk;
using Gee;
using Notify;

namespace Worldinfo {

	private HashMap<TargetName, string> last_response_etag;

	private ArrayList<string> news_ids;
	private ArrayList<string> events_ids;
	private ArrayList<string> sortie_ids;
	private ArrayList<string> syndicte_mission_ids;
	private ArrayList<string> fissure_ids;
	private ArrayList<string> invasion_ids;
	private string outpost_id;
	private ArrayList<string> arbitration_ids;
	private ArrayList<string> nightwave_mission_ids;

	public Gtk.Application app;

	private const string api_path = "http://api.warframestat.us/pc";
	public Json.Node? get_info(){
		var session = new Session();
		var request_message = new Message("GET", api_path);

		var parser = new Json.Parser();

		try{
			var inputStream = session.send(request_message);

			parser.load_from_stream(inputStream);

			return parser.get_root();
		}catch (Error e) {
			warning("Failed to fetch data with error: %s", e.message);
		}

		return null;
	}

	public Json.Node? get_direct_info(Watcher.TargetName target_name){
		var target = "";
		switch (target_name) {
		case TargetName.NEWS:
			target = "news";
			break;
		case TargetName.EVENTS:
			target = "events";
			break;
		case TargetName.SORTIE:
			target = "sortie";
			break;
		case TargetName.SYNDICATES:
			target = "syndicateMissions";
			break;
		case TargetName.FISSURES:
			target = "fissures";
			break;
		case TargetName.INVASIONS:
			target = "invasions";
			break;
		case TargetName.EARTH:
			target = "earthCycle";
			break;
		case TargetName.CETUS:
			target = "cetusCycle";
			break;
		case TargetName.VALLIS:
			target = "vallisCycle";
			break;
		case TargetName.OUTPOSTS:
			target = "sentientOutposts";
			break;
		case TargetName.ARBITRATION:
			target = "arbitration";
			break;
		case TargetName.NIGHTWAVE:
			target = "nightwave";
			break;
		}

		var session = new Session();
		var request_message = new Message("GET", api_path + "/" + target);
		if (last_response_etag != null && last_response_etag.get(target_name) != null)
			request_message.request_headers.append("if-None-Match", last_response_etag.get(target_name));

		var parser = new Json.Parser();

		try{
			var inputStream = session.send(request_message);
			if (last_response_etag == null)
				last_response_etag = new HashMap<TargetName, string>();
			last_response_etag.set(target_name, request_message.response_headers.get_one("etag"));
			if (request_message.status_code == 200) {
				parser.load_from_stream(inputStream);
			}
			return parser.get_root();
		}catch (Error e) {
			warning("Failed to fetch data with error: %s", e.message);
		}

		return null;
	}

	/**
	   see https://docs.warframestat.us/#tag/Worldstate/paths/~1{platform}/get for reference
	 */
	public Gtk.Widget? get_news(){
		var news_node = get_direct_info(TargetName.NEWS);

		if (news_node != null) {
			var list = new Gtk.ListBox();

			foreach (var sub_node in news_node.get_array().get_elements()) {
				var row = new Gtk.ListBoxRow();

				var news_object = sub_node.get_object();
				var date = news_object.get_string_member("date");
				var image_link = news_object.get_string_member("imageLink");
				var eta = news_object.get_string_member("eta");
				var prime_access = news_object.get_boolean_member("primeAccess");
				var stream = news_object.get_boolean_member("stream");
				var link = news_object.get_string_member("link");
				var update = news_object.get_boolean_member("update");
				var message = news_object.get_string_member("message");
				var priority = news_object.get_boolean_member("priority");

				var root_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
				var primary_hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
				var secondary_hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

				root_box.add(primary_hbox);
				root_box.add(secondary_hbox);

				var id = news_object.get_string_member("id");
				if (news_ids == null)
					news_ids = new ArrayList<string>();
				if ((!news_ids.contains(id)) && app != null) {
					news_ids.add(id);
					//var notification = new Notification("There are news!");
					//notification.set_body("This is a test notification!");
					try {
						var notification = new Notify.Notification("There are news", _f(message), null);
						Notify.init("cephalon-6015");
						notification.show();
					} catch (Error e) {
						critical("%s", e.message);
					}
				}else if (app == null)
					GLib.message("There is no app to send Notifications from!");

				var date_label = new Gtk.Label(_f(date));
				var eta_label = new Gtk.Label(_f(eta));

				var dynamic_eta = new SelfUpdateingProperty(date);
				GLib.Timeout.add_seconds(1, () => {dynamic_eta.shedule(); eta_label.set_label(dynamic_eta.get_initial_value()); return true;});

				var message_label = new Gtk.Label(_f(message));
				var link_button = new Gtk.LinkButton.with_label(link, "");
				var browser_image = new Gtk.Image.from_icon_name("web-browser-symbolic", Gtk.IconSize.BUTTON);
				link_button.set_image(browser_image);
				link_button.set_always_show_image(true);
				message_label.set_line_wrap(true);

				primary_hbox.add(eta_label);
				primary_hbox.add(message_label);
				secondary_hbox.pack_start(link_button, false, false);
				secondary_hbox.pack_end(date_label, false, true);

				if (prime_access) {
					link_button.set_label(_f("primeAccess"));
				}else if (update) {
					link_button.set_label(_f("update"));
				}else if (stream) {
					link_button.set_label(_f("stream"));
				}


				row.add(root_box);
				list.add(row);
			}
			return list;
		}
		return null;
	}

	public Gtk.Widget? get_events(){
		var events_node = get_direct_info(TargetName.EVENTS);

		if (events_node != null) {
			var list = new Gtk.ListBox();

			foreach (var sub_node in events_node.get_array().get_elements()) {
				var row = new Gtk.ListBoxRow();

				var events_object = sub_node.get_object();

				var description = events_object.get_string_member("description");
				var rewards_object = events_object.get_array_member("rewards");
				var node = events_object.get_string_member("node");
				var interim_steps = events_object.get_array_member("interimSteps");
				var maximum_score = events_object.get_int_member("maximumScore");
				var current_score = events_object.get_int_member("currentScore");

				var root_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
				var primary_hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
				var secondary_hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
				var tertiary_vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);

				root_box.add(primary_hbox);
				root_box.add(secondary_hbox);
				root_box.add(tertiary_vbox);

				var id = events_object.get_string_member("id");
				if (events_ids == null)
					events_ids = new ArrayList<string>();
				if ((!events_ids.contains(id)) && app != null) {
					events_ids.add(id);
					try {
						var notification = new Notify.Notification("There are new Events!", _f(description), null);
						Notify.init("cephalon-6015");
						notification.show();
					} catch (Error e) {
						critical("%s", e.message);
					}
				}else if (app == null)
					GLib.message("There is no app to send Notifications from!");

				var description_label = new Gtk.Label(_f(description));
				var node_label = new Gtk.Label(_f(node));
				description_label.set_line_wrap(true);

				var score = new Gtk.LevelBar();
				score.set_value(current_score);
				score.set_min_value(0);
				score.set_max_value(maximum_score);

				primary_hbox.add(description_label);
				secondary_hbox.pack_start(node_label, false, false);
				tertiary_vbox.add(get_rewards(rewards_object));
				tertiary_vbox.add(score);

				var steps_label = new Gtk.Label(_f("Archivements"));
				tertiary_vbox.add(steps_label);

				var steps_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
				var steps_stack = get_steps(interim_steps) as Gtk.Stack;
				var stack_switcher = new Gtk.StackSwitcher();
				stack_switcher.set_stack(steps_stack);
				stack_switcher.set_orientation(Gtk.Orientation.VERTICAL);
				steps_box.add(stack_switcher);
				steps_box.add(steps_stack);
				tertiary_vbox.add(steps_box);

				row.add(root_box);
				list.add(row);
			}
			return list;
		}
		return null;
	}

	public Gtk.Widget? get_sortie(){
		var sortie_node = get_direct_info(TargetName.SORTIE);

		if (sortie_node != null) {
			var list = new Gtk.ListBox();

			var row = new Gtk.ListBoxRow();

			var data_object = sortie_node.get_object();

			var eta = data_object.get_string_member("expiry");
			var variants = data_object.get_array_member("variants");

			var root_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			var primary_hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			var secondary_vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);

			root_box.add(primary_hbox);
			root_box.add(secondary_vbox);

			var id = data_object.get_string_member("id");
			if (sortie_ids == null)
				sortie_ids = new ArrayList<string>();
			if ((!sortie_ids.contains(id)) && app != null) {
				sortie_ids.add(id);
				try {
					var notification = new Notify.Notification("There is a new Sortie!", null, null);
					Notify.init("cephalon-6015");
					notification.show();
				} catch (Error e) {
					critical("%s", e.message);
				}
			}else if (app == null)
				GLib.message("There is no app to send Notifications from!");

			var eta_label = new Gtk.Label(_f(eta));

			var dynamic_eta = new SelfUpdateingProperty(eta, false);
			Timeout.add_seconds(1, () => {dynamic_eta.shedule(); eta_label.set_label(dynamic_eta.get_initial_value()); return true;});

			var variants_stack = get_variants(variants) as Gtk.Stack;
			var variants_switcher = new Gtk.StackSwitcher();
			variants_switcher.set_stack(variants_stack);

			primary_hbox.add(eta_label);
			secondary_vbox.add(variants_switcher);
			secondary_vbox.add(variants_stack);

			row.add(root_box);
			list.add(row);
			return list;
		}
		return null;
	}

	public Gtk.Widget? get_syndicates(){
		var syndicate_node = get_direct_info(TargetName.SYNDICATES);

		if (syndicate_node != null) {

			var data_array = syndicate_node.get_array();

			var hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

			var syndicates_stack = new Gtk.Stack();
			var switcher = new Gtk.StackSwitcher();
			switcher.set_stack(syndicates_stack);
			switcher.set_orientation(Gtk.Orientation.VERTICAL);

			hbox.add(switcher);
			hbox.add(syndicates_stack);

			foreach (var syndicate in data_array.get_elements()) {
				var syndicate_name = syndicate.get_object().get_string_member("syndicate");
				var nodes = syndicate.get_object().get_array_member("nodes");
				var eta = syndicate.get_object().get_string_member("expiry");
				var jobs = syndicate.get_object().get_array_member("jobs");

				var primary_vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);

				var eta_label = new Gtk.Label(_f("ETA: " + eta));
				var dynamic_eta = new SelfUpdateingProperty(eta, false);
				Timeout.add_seconds(1, () => {dynamic_eta.shedule(); eta_label.set_label(dynamic_eta.get_initial_value()); return true;});
				primary_vbox.add(eta_label);

				var id = syndicate.get_object().get_string_member("id");
				if (syndicte_mission_ids == null)
					syndicte_mission_ids = new ArrayList<string>();
				if ((!syndicte_mission_ids.contains(id)) && app != null) {
					syndicte_mission_ids.add(id);
					try {
						var notification = new Notify.Notification("There are new Events!", _f(syndicate_name + ", expires in" + eta), null);
						Notify.init("cephalon-6015");
						notification.show();
					} catch (Error e) {
						critical("%s", e.message);
					}
				}else if (app == null)
					GLib.message("There is no app to send Notifications from!");

				if (nodes.get_length() == 0 && jobs.get_length() == 0) {
					continue;
				}else if (nodes.get_length() != 0) {
					var nodes_list = get_nodes(nodes);
					primary_vbox.add(nodes_list);
				}else if (jobs.get_length() != 0) {
					var jobs_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
					var jobs_stack = get_jobs(jobs) as Gtk.Stack;
					var jobs_switcher = new Gtk.StackSwitcher();
					jobs_switcher.set_orientation(Gtk.Orientation.VERTICAL);
					jobs_switcher.set_stack(jobs_stack);
					jobs_box.add(jobs_switcher);
					jobs_box.add(jobs_stack);
					primary_vbox.add(jobs_box);
				}
				syndicates_stack.add_titled(primary_vbox, syndicate_name, _f(syndicate_name));
			}
			return hbox;
		}
		return null;
	}

	public Widget? get_fissures(){
		var fissures_node = get_direct_info(TargetName.FISSURES);

		if (fissures_node != null) {
			var fissures_array = fissures_node.get_array();

			var fissures_stack = new Stack();
			var switcher = new StackSwitcher();
			switcher.set_stack(fissures_stack);
			switcher.set_orientation(Orientation.VERTICAL);

			var fissures_box = new Box(Orientation.HORIZONTAL, 10);
			fissures_box.add(switcher);
			fissures_box.add(fissures_stack);

			var lith_list = new ListBox();
			var meso_list = new ListBox();
			var neo_list = new ListBox();
			var axi_list = new ListBox();
			var requiem_list = new ListBox();

			fissures_stack.add_titled(lith_list, "lith", _f("lith"));
			fissures_stack.add_titled(meso_list, "meso", _f("meso"));
			fissures_stack.add_titled(neo_list, "neo", _f("neo"));
			fissures_stack.add_titled(axi_list, "axi", _f("axi"));
			fissures_stack.add_titled(requiem_list, "requiem", _f("requiem"));

			foreach (var fissure in fissures_array.get_elements()) {
				var node = fissure.get_object().get_string_member("node");
				var mission_type = fissure.get_object().get_string_member("missionType");
				var enemy = fissure.get_object().get_string_member("enemy");
				var tier_num = fissure.get_object().get_int_member("tierNum");
				var eta = fissure.get_object().get_string_member("expiry");

				var node_label = new Label(_f("node: " + node));
				var mission_type_label = new Label(_f("type: " + mission_type));
				var enemy_label = new Label(_f("Enemy: " + enemy));
				var eta_label = new Label(_f("ETA: " + eta));

				var id = fissure.get_object().get_string_member("id");
				if (fissure_ids == null)
					fissure_ids = new ArrayList<string>();
				if ((!fissure_ids.contains(id)) && app != null) {
					fissure_ids.add(id);
					try {
						var notification = new Notify.Notification("There are new Fissures!", null, null); // TODO Add additional info here …
						Notify.init("cephalon-6015");
						notification.show();
					} catch (Error e) {
						critical("%s", e.message);
					}
				}else if (app == null)
					GLib.message("There is no app to send Notifications from!");

				var dynamic_eta = new SelfUpdateingProperty(eta, false);
				Timeout.add_seconds(1, () => {dynamic_eta.shedule(); eta_label.set_label(dynamic_eta.get_initial_value()); return true;});

				var box = new Box(Orientation.VERTICAL, 10);
				box.add(eta_label);
				box.add(node_label);
				box.add(mission_type_label);
				box.add(enemy_label);

				var row = new ListBoxRow();
				row.add(box);

				switch (tier_num) {
				case 1:
					lith_list.add(row);
					break;
				case 2:
					meso_list.add(row);
					break;
				case 3:
					neo_list.add(row);
					break;
				case 4:
					axi_list.add(row);
					break;
				case 5:
					requiem_list.add(row);
					break;
				}
			}
			return fissures_box;
		} return null;
	}

	public Widget? get_invasions(){
		var invasions_node = get_direct_info(TargetName.INVASIONS);

		if (invasions_node != null) {
			var invasions_list = new ListBox();

			var invasions_array = invasions_node.get_array();

			foreach (var invasion in invasions_array.get_elements()) {
				var node = invasion.get_object().get_string_member("node");
				var desc = invasion.get_object().get_string_member("desc");
				var attacking_faction = invasion.get_object().get_string_member("attackingFaction");
				var defending_faction = invasion.get_object().get_string_member("defendingFaction");
				var vs_infestation = invasion.get_object().get_boolean_member("vsInfestation");
				var count = invasion.get_object().get_int_member("count");
				var required_runs = invasion.get_object().get_int_member("requiredRuns");
				var completed = invasion.get_object().get_boolean_member("completed");
				var eta = invasion.get_object().get_string_member("activation");

				if (completed)
					continue;

				var rewards_box = new Box(Orientation.HORIZONTAL, 10);
				if (!vs_infestation) {
					var attacking_faction_reward = invasion.get_object().get_object_member("attackerReward").get_string_member("asString");
					var attacking_faction_reward_label = new Label(_f(attacking_faction_reward));
					rewards_box.pack_start(attacking_faction_reward_label);
				}
				var defending_faction_reward = invasion.get_object().get_object_member("defenderReward").get_string_member("asString");
				var defending_faction_reward_label = new Label(_f(defending_faction_reward));
				rewards_box.pack_end(defending_faction_reward_label);

				var id = invasion.get_object().get_string_member("id");
				if (invasion_ids == null)
					invasion_ids = new ArrayList<string>();
				if ((!invasion_ids.contains(id)) && app != null) {
					invasion_ids.add(id);
					try {
						var notification = new Notify.Notification("There are new Invasions!", _f(node), null); // TODO Actually we will add additional info to almost every notification - soon™️
						Notify.init("cephalon-6015");
						notification.show();
					} catch (Error e) {
						critical("%s", e.message);
					}
				}else if (app == null)
					GLib.message("There is no app to send Notifications from!");

				var node_label = new Label(_f("Node: " + node));
				var desc_label = new Label(_f("Description: " + desc));
				var attacking_faction_label = new Label(_f("Attacking Faction: " + attacking_faction));
				var defending_faction_label = new Label(_f("Defending Faction: " + defending_faction));
				var eta_label = new Label(_f("ETA: " + eta));


				var dynamic_eta = new SelfUpdateingProperty(eta);
				dynamic_eta.shedule(); eta_label.set_label(dynamic_eta.get_initial_value());
				Timeout.add_seconds(1, () => {dynamic_eta.shedule(); eta_label.set_label(dynamic_eta.get_initial_value()); return true;});


				var level = new LevelBar();
				level.set_min_value(0);
				level.set_max_value(required_runs);
				level.set_value(required_runs / 2 + count);

				var faction_box = new Box(Orientation.HORIZONTAL, 10);
				faction_box.pack_start(attacking_faction_label);
				faction_box.pack_end(defending_faction_label);

				var row = new ListBoxRow();
				var invasion_box = new Box(Orientation.VERTICAL, 10);
				invasion_box.add(node_label);
				invasion_box.add(desc_label);
				invasion_box.add(eta_label);
				invasion_box.add(faction_box);
				invasion_box.add(level);
				invasion_box.add(rewards_box);
				row.add(invasion_box);
				invasions_list.add(row);
			}
			return invasions_list;
		}
		return null;
	}

	public Widget? get_earth(){
		var earth_node = get_direct_info(TargetName.EARTH);

		if (earth_node != null) {
			var state = earth_node.get_object().get_string_member("state");
			var time_left = earth_node.get_object().get_string_member("expiry");
			var label = new Label(_f(state + " for next " + time_left));

			var dynamic_eta = new SelfUpdateingProperty(time_left, false, false);
			Timeout.add_seconds(1, () => {dynamic_eta.shedule(); label.set_label(_f(state + " for next " + dynamic_eta.get_initial_value())); return true;});

			return label;
		} return null;
	}

	public Widget? get_open_world(TargetName target){
		var world_node = get_direct_info(target);

		if (world_node != null) {
			//var info = world_node.get_object().get_string_member("shortString");
			var expiry = world_node.get_object().get_string_member("expiry");
			var state = world_node.get_object().get_string_member("state");
			var info_label = new Label(null);

			var dynamic_eta = new SelfUpdateingProperty(expiry, false, false);
			dynamic_eta.shedule();
			info_label.set_label(_f(state + " for next " + dynamic_eta.get_initial_value()));
			Timeout.add_seconds(1, () => {dynamic_eta.shedule(); info_label.set_label(_f(state + " for next " + dynamic_eta.get_initial_value())); return true;});

			return info_label;
		} return null;
	}

	public Widget? get_outposts(){
		var outposts_node = get_direct_info(TargetName.OUTPOSTS);

		if (outposts_node != null) {
			var node = outposts_node.get_object().get_object_member("mission").get_string_member("node");
			var faction = outposts_node.get_object().get_object_member("mission").get_string_member("faction");
			var type = outposts_node.get_object().get_object_member("mission").get_string_member("type");

			var id = outposts_node.get_object().get_string_member("id");
			if (outpost_id == null)
				outpost_id = "";
			if ((outpost_id !=id) && app != null) {
				outpost_id = id;
				//var notification = new Notification("There are news!");
				//notification.set_body("This is a test notification!");
				try {
					var notification = new Notify.Notification("There is a new Outpost", _f(node), null);
					Notify.init("cephalon-6015");
					notification.show();
				} catch (Error e) {
					critical("%s", e.message);
				}
			}else if (app == null)
				GLib.message("There is no app to send Notifications from!");

			var node_label = new Label(_f("node: " + node));
			var faction_label = new Label(_f("faction: " + faction));
			var type_label = new Label(_f("type: " + type));

			var info_box = new Box(Orientation.VERTICAL, 10);
			info_box.add(node_label);
			info_box.add(faction_label);
			info_box.add(type_label);
			return info_box;
		} return null;
	}

	public Widget? get_nightwave(){
		var nightwave_node = get_direct_info(TargetName.NIGHTWAVE);

		if (nightwave_node != null) {
			var active_challenges_array = nightwave_node.get_object().get_array_member("activeChallenges");

			var daily_list = new ListBox();
			var weekly_list = new ListBox();
			var elite_list = new ListBox();

			foreach (var challenge in active_challenges_array.get_elements() ) {
				var is_daily = false;
				if (challenge.get_object().has_member("isDaily"))         //Daily info is not provided if the mission is not daily - however we set it to the value if it's present
					is_daily = challenge.get_object().get_boolean_member("isDaily");
				var is_elite = challenge.get_object().get_boolean_member("isElite");
				var desc = challenge.get_object().get_string_member("desc");
				var title = challenge.get_object().get_string_member("title");
				var reputation = challenge.get_object().get_int_member("reputation");

				var id = challenge.get_object().get_string_member("id");
				if (nightwave_mission_ids == null)
					nightwave_mission_ids = new ArrayList<string>();
				if ((!nightwave_mission_ids.contains(id)) && app != null) {
					nightwave_mission_ids.add(id);
					//var notification = new Notification("There are news!");
					//notification.set_body("This is a test notification!");
					try {
						var notification = new Notify.Notification("There are new Nightwave acts!", _f(desc), null);
						Notify.init("cephalon-6015");
						notification.show();
					} catch (Error e) {
						critical("%s", e.message);
					}
				}else if (app == null)
					GLib.message("There is no app to send Notifications from!");

				var desc_label = new Label(_f(desc));
				var title_label = new Label(_f(title));
				var reputation_label = new Label(_f("Standing: " + reputation.to_string()));

				var box = new Box(Orientation.VERTICAL, 10);
				box.add(title_label);
				box.add(desc_label);
				box.add(reputation_label);

				if (is_daily) {
					var row = new ListBoxRow();
					row.add(box);
					daily_list.add(row);
				}else if (is_elite) {
					var row = new ListBoxRow();
					row.add(box);
					elite_list.add(row);
				}else {
					var row = new ListBoxRow();
					row.add(box);
					weekly_list.add(row);
				}
			}

			var stack = new Stack();
			var switcher = new StackSwitcher();
			switcher.set_stack(stack);

			stack.add_titled(daily_list, "daily", _f("daily"));
			stack.add_titled(weekly_list, "weekly", _f("weekly"));
			stack.add_titled(elite_list, "elite", _f("elite"));

			var nightwave_box = new Box(Orientation.VERTICAL, 10);
			nightwave_box.add(switcher);
			nightwave_box.add(stack);
			return nightwave_box;
		} return null;
	}

	public Widget? get_arbitration(){
		var outposts_node = get_direct_info(TargetName.ARBITRATION);

		if (outposts_node != null) {
			var enemy = outposts_node.get_object().get_string_member("enemy");
			var node = outposts_node.get_object().get_string_member("node");
			var type = outposts_node.get_object().get_string_member("type");

			// TODO Find a way to display notifications about arbitrations

			var node_label = new Label(_f("node: " + node));
			var enemy_label = new Label(_f("enemy: " + enemy));
			var type_label = new Label(_f("type: " + type));

			var info_box = new Box(Orientation.VERTICAL, 10);
			info_box.add(node_label);
			info_box.add(enemy_label);
			info_box.add(type_label);
			return info_box;
		} return null;
	}

	private Gtk.Widget get_rewards(Json.Array rewards_object){
		var rewards_list = new Gtk.ListBox();
		foreach (var reward in rewards_object.get_elements()) {
			var reward_string = reward.get_object().get_string_member("asString");
			if (reward_string != "") {
				var row = new Gtk.ListBoxRow();
				var rewards_label = new Gtk.Label(reward_string);
				rewards_label.set_line_wrap(true);
				row.add(rewards_label);
				rewards_list.add(row);
			}
		}
		return rewards_list;
	}

	private Gtk.Widget get_steps(Json.Array steps){
		var steps_stack = new Gtk.Stack();
		foreach (var step in steps.get_elements()) {
			var goal = step.get_object().get_int_member("goal");
			var reward = step.get_object().get_object_member("reward").get_string_member("asString");
			var reward_label = new Gtk.Label(reward);
			reward_label.set_line_wrap(true);
			steps_stack.add_titled(reward_label, goal.to_string(), _f(goal.to_string()));
		}
		return steps_stack;
	}

	private Gtk.Widget get_variants(Json.Array steps){
		var variants_stack = new Gtk.Stack();
		foreach (var step in steps.get_elements()) {
			var vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			var mission_type = step.get_object().get_string_member("missionType");
			var modifier = step.get_object().get_string_member("modifier");
			var modifier_description = step.get_object().get_string_member("modifierDescription");
			var node = step.get_object().get_string_member("node");
			var modifier_label = new Gtk.Label(modifier);
			var modifier_description_label = new Gtk.Label(modifier_description);
			var node_label = new Gtk.Label(node);
			modifier_description_label.set_line_wrap(true);
			vbox.add(node_label);
			vbox.add(modifier_label);
			vbox.add(modifier_description_label);
			variants_stack.add_titled(vbox, mission_type, _f(mission_type));
		}
		return variants_stack;
	}

	private Gtk.Widget get_nodes(Json.Array nodes){
		var nodes_list = new Gtk.ListBox();
		foreach (var node in nodes.get_elements()) {
			var row = new Gtk.ListBoxRow();
			var node_label = new Gtk.Label(_f(node.get_string()));
			row.add(node_label);
			nodes_list.add(row);
		}
		return nodes_list;
	}

	private Gtk.Widget get_jobs(Json.Array jobs){
		var jobs_stack = new Gtk.Stack();
		foreach (var job in jobs.get_elements()) {
			var vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			var hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

			var rewards_list = new Gtk.ListBox();
			var reward_pool = job.get_object().get_array_member("rewardPool");
			foreach (var reward in reward_pool.get_elements()) {
				var row = new Gtk.ListBoxRow();
				var reward_label = new Gtk.Label(_f(reward.get_string()));
				row.add(reward_label);
				rewards_list.add(row);
			}
			var type = job.get_object().get_string_member("type");
			var enemy_lvl_min = job.get_object().get_array_member("enemyLevels").get_int_element(0);
			var enemy_lvl_max = job.get_object().get_array_member("enemyLevels").get_int_element(1);
			var standing_stages = job.get_object().get_array_member("standingStages");
			var standing_list = new Gtk.ListBox();
			foreach (var stage in standing_stages.get_elements()) {
				var row = new Gtk.ListBoxRow();
				var standing_label = new Gtk.Label(_f(stage.get_int().to_string()));
				row.add(standing_label);
				standing_list.add(row);
			}
			var min_mr = job.get_object().get_int_member("minMR");

			var min_mr_label = new Gtk.Label(_f("Minimal MR: " + min_mr.to_string()));

			var rewards_desc_label = new Gtk.Label(_f("rewards:"));
			var rewards_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			rewards_box.add(rewards_desc_label);
			rewards_box.add(rewards_list);

			var standing_desc_label = new Gtk.Label(_f("standing per stage:"));
			var standing_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
			standing_box.add(standing_desc_label);
			standing_box.add(standing_list);

			hbox.add(rewards_box);
			hbox.add(standing_box);
			vbox.add(hbox);
			vbox.add(min_mr_label);

			jobs_stack.add_titled(vbox, type, _f(type + " (" + enemy_lvl_min.to_string() + " – " + enemy_lvl_max.to_string() + ")"));
		}
		return jobs_stack;
	}

	private string _f(string str){
		return Reformatter.format(str);
	}

	public Widget? get_info_by_name(TargetName name){
		switch (name) {
		case TargetName.NEWS:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_news();});
				thread_data.got_element.connect((widget) => {message("got notification from news");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for news", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.ARBITRATION:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_arbitration();});
				thread_data.got_element.connect((widget) => {message("got notification from invasions");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for invasions", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.CETUS:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_open_world(name);});
				thread_data.got_element.connect((widget) => {message("got notification from cetus");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for cetus", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.EARTH:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_earth();});
				thread_data.got_element.connect((widget) => {message("got notification from earth");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for earth", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.EVENTS:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_events();});
				thread_data.got_element.connect((widget) => {message("got notification from events");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for events", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.FISSURES:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_fissures();});
				thread_data.got_element.connect((widget) => {message("got notification from fissures");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for fissures", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.INVASIONS:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_invasions();});
				thread_data.got_element.connect((widget) => {message("got notification from invasions");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for invasions", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.NIGHTWAVE:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_nightwave();});
				thread_data.got_element.connect((widget) => {message("got notification from nightwave");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for nightwave", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.OUTPOSTS:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_outposts();});
				thread_data.got_element.connect((widget) => {message("got notification from outposts");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for outposts", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.SORTIE:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_sortie();});
				thread_data.got_element.connect((widget) => {message("got notification from sortie");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for sortie", thread_data.shedule);
			} catch (Error e) {warning(e.message);}
			return self_updating_box;
		case TargetName.SYNDICATES:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_syndicates();});
				thread_data.got_element.connect((widget) => {message("got notification for syndicates");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher for syndicates", thread_data.shedule);
			} catch (Error e) {
				warning("%s", e.message);
			}
			return self_updating_box;
		case TargetName.VALLIS:
			var self_updating_box = new Box(Orientation.HORIZONTAL, 0);
			try {
				var thread_data = new MethodWorker(10, () => {return get_open_world(name);});
				thread_data.got_element.connect((widget) => {message("got notification for vallis");
				                                             build_info(widget, self_updating_box);});
				Thread<void> thread = new Thread<void>.try ("background worker data fetcher", thread_data.shedule);
			} catch (Error e) {
				warning("%s", e.message);
			}
			return self_updating_box;
		default:
			return new Label(_f("something gone wrong. :("));
		}
	}

	private Gtk.Box build_info(Gtk.Widget? info, Gtk.Box self_updating_box){
		if (info != null) {
			Idle.add(() => {
				self_updating_box.get_children().foreach(c => self_updating_box.remove(c));
				self_updating_box.add(info);
				info.show_all();
				return false;
			});
		}
		return self_updating_box;
	}
}
