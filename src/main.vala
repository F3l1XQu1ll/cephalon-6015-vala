using Gtk;
using Search;
using Cache;
using Gee;
using Reformatter;
using Watcher;
using Notify;

public interface INFO_STRINGS {
	public const string NEWS = "news";
	public const string EVENTS = "events";
	public const string SORTIE = "sortie";
	public const string SYNDICATES = "syndicates";
	public const string FISSURES = "fissures";
	public const string INVASIONS = "invasions";
	public const string EARTH = "earth";
	public const string CETUS = "cetus";
	public const string VALLIS = "vallis";
	public const string OUTPOSTS = "outposts";
	public const string ARBITRATION = "arbitration";
	public const string NIGHTWAVE = "nightwave";
}

public class MainApplication : Gtk.Application {

	public MainApplication()
	{
		Object(application_id: "com.archyt.cephalon-6015", flags : ApplicationFlags.FLAGS_NONE);
	}

	protected override void activate()
	{
		/*
		   Init Cache - DO NOT RUN UNTIL APP IS INITIALIZED!!!
		 */
		Cache.init_name_cache();

		register_target(TargetName.NEWS, null);
		//start_watcher();

		Worldinfo.app = this;

		var main_window = new window();

		var screen = Gdk.Screen.get_default();
		var provider = new Gtk.CssProvider();
		provider.load_from_resource("/ui/custom_gtk.css");
		Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

		Gtk.Settings.get_default().gtk_theme_name = "Adwaita";
		Gtk.Settings.get_default().set_property("gtk-application-prefer-dark-theme", true);
		main_window.set_application(this);
		main_window.present();
	}

	public static int main(string[] args){
		/*
		   Start main app
		 */
		MainApplication app = new MainApplication();
		return(app.run(args));
	}
}

[GtkTemplate(ui = "/ui/window.ui")]
public class window : ApplicationWindow {
	[GtkChild]
	private SearchEntry se_default;
	[GtkChild]
	private Stack stack;


	private Gtk.ListStore list_store = new Gtk.ListStore(1, Type.STRING);
	private Gtk.EntryCompletion completion = new Gtk.EntryCompletion();

	public window()
	{
		completion.set_inline_completion(true);
		completion.set_minimum_key_length(3);
		completion.set_text_column(0);
		se_default.set_completion(completion);
	}

	[GtkCallback]
	private void on_se_default_search_changed(Entry entry)
	{
		if (entry.get_text().length >= 3) {
			update_suggestions(entry.get_text());
		}
	}

	[GtkCallback]
	private void on_btn_refresh_clicked(Button button)
	{
		Cache.fetch_data();
	}

	[GtkCallback]
	private void on_button_settings_clicked(Button button)
	{
		var win = new AppSettingsWindow();
		win.show_all();
	}

	[GtkCallback]
	private void on_btn_about_clicked(Button button){
		//this.get_application().send_notification("test", new Notification("Hello World"));
		var builder = new Builder();
		try{
			builder.add_from_resource("/ui/about.ui");
			var about_dialog = builder.get_object("about") as AboutDialog;
			about_dialog.run();
			about_dialog.hide();
		}catch (Error e) {
			critical("%s", e.message);
		}

	}

	[GtkCallback]
	private void on_btn_search_clicked(Button button)
	{
		foreach(var child in stack.get_children()) {
			stack.remove(child);
		}

		switch (se_default.get_text()) {
		case "news":
			add_to_stack(TargetName.NEWS, INFO_STRINGS.NEWS);
			try {
				var thread_data = new UpdateWorker(5);
				thread_data.update_event.connect(() => {message("got notification");});
				Thread<void> thread = new Thread<void>.try ("background worker", thread_data.shedule);
									   //thread.join();
			} catch (Error e) {
				warning("%s", e.message);
			}
			break;
		case "events":
			add_to_stack(TargetName.EVENTS, INFO_STRINGS.EVENTS);
			break;
		case "sortie":
			add_to_stack(TargetName.SORTIE, INFO_STRINGS.SORTIE);
			break;
		case "syndicates":
			add_to_stack(TargetName.SYNDICATES, INFO_STRINGS.SYNDICATES);
			break;
		case "fissures":
			add_to_stack(TargetName.FISSURES, INFO_STRINGS.FISSURES);
			break;
		case "invasions":
			add_to_stack(TargetName.INVASIONS, INFO_STRINGS.INVASIONS);
			break;
		case "earth":
			add_to_stack(TargetName.EARTH, INFO_STRINGS.EARTH);
			break;
		case "cetus":
			add_to_stack(TargetName.CETUS, INFO_STRINGS.CETUS);
			break;
		case "vallis":
			add_to_stack(TargetName.VALLIS, INFO_STRINGS.VALLIS);
			break;
		case "outposts":
			add_to_stack(TargetName.OUTPOSTS, INFO_STRINGS.OUTPOSTS);
			break;
		case "arbitration":
			add_to_stack(TargetName.ARBITRATION, INFO_STRINGS.ARBITRATION);
			break;
		case "nightwave":
			add_to_stack(TargetName.NIGHTWAVE, INFO_STRINGS.NIGHTWAVE);
			break;
		case "world":
			add_to_stack(TargetName.NEWS, INFO_STRINGS.NEWS);
			add_to_stack(TargetName.EVENTS, INFO_STRINGS.EVENTS);
			add_to_stack(TargetName.SORTIE, INFO_STRINGS.SORTIE);
			add_to_stack(TargetName.SYNDICATES, INFO_STRINGS.SYNDICATES);
			add_to_stack(TargetName.FISSURES, INFO_STRINGS.FISSURES);
			add_to_stack(TargetName.INVASIONS, INFO_STRINGS.INVASIONS);
			add_to_stack(TargetName.EARTH, INFO_STRINGS.EARTH);
			add_to_stack(TargetName.CETUS, INFO_STRINGS.CETUS);
			add_to_stack(TargetName.VALLIS, INFO_STRINGS.VALLIS);
			add_to_stack(TargetName.OUTPOSTS, INFO_STRINGS.OUTPOSTS);
			add_to_stack(TargetName.ARBITRATION, INFO_STRINGS.ARBITRATION);
			add_to_stack(TargetName.NIGHTWAVE, INFO_STRINGS.NIGHTWAVE);
			break;
		default:
			var result = Search.do_search(se_default.get_text());
			//message("%s", result.type_name());
			//display_tree(result);

			var request_parts = se_default.get_text().split(">");
			if (request_parts != null)
				fill_stack(result, null, request_parts[request_parts.length - 1]);
			else
				fill_stack(result, null, null);
			break;
		}
		this.show_all();
	}

	[GtkCallback]
	private void on_toggle_on_top_toggled(ToggleButton button){
        if (button.get_active())
            this.set_keep_above(true);
        else
            this.set_keep_above(false);
    }

	[GtkCallback]
	private void on_btn_quit_clicked(Button button)
	{
		this.destroy();
	}

/*private void display_values(string value){
        var value_view = new Gtk.TextView();
        value_view.get_buffer().set_text(_f(value));
        value_view.set_wrap_mode(Gtk.WrapMode.WORD);
        listbox_results.insert(value_view, -1);
   }*/

/*[GtkCallback]
   private void on_tree_selection_changed(Gtk.TreeSelection selection){
        TreeModel model;
        Gtk.TreeIter iter;
        selection.get_selected(out model, out iter);
        string value;
        if (selection.get_selected(out model, out iter)) {
                model.get(iter, 1, out value, -1);

                foreach (var child  in listbox_results.get_children()) {
                        child.destroy();
                }
                //message(value);

                display_values(value);
                this.show_all();
        }
   }*/

	private void update_suggestions(string text){
		ArrayList<string> suggestions = Search.get_completions(text);
		if (!(suggestions.is_empty) && !(suggestions == null)) {
			list_store.clear();
			foreach(var suggestion in suggestions) {
				if (suggestion.has_prefix(text)) {
					var iter = TreeIter();
					list_store.append(out iter);
					list_store.set_value(iter, 0, suggestion);
					completion.set_model(list_store);
				}
			}
			completion.complete();
		}
	}

/*private void display_tree(Json.Node results){
        message("prepairing tree …");

        foreach (var column in results_tree.get_columns()) {
                results_tree.remove_column(column);
        }

        var tree_store = new Gtk.TreeStore(2, Type.STRING, Type.STRING);

        var treeiter = TreeIter();

        walk_tree(results, tree_store);
        results_tree.set_model(tree_store);

        var renderer = new Gtk.CellRendererText();
        var column = results_tree.insert_column_with_attributes(-1, "Attributes", renderer, "text", 0, null);

        var hidden_renderer = new Gtk.CellRendererText();
        var hidden_column = results_tree.insert_column_with_attributes(-1, "Value", hidden_renderer, null);
        results_tree.get_column(hidden_column - 1).set_visible(false);

        fill_stack(results, null);

        this.show_all();
   }*/

/*private void walk_tree(Json.Node content, TreeStore store, TreeIter? parent = null){
        if (content.get_node_type() == Json.NodeType.OBJECT) {
                foreach (var member in content.get_object().get_members()) {
                        if ((content.get_object().get_member(member).get_node_type() == Json.NodeType.OBJECT) ||
                            (content.get_object().get_member(member).get_node_type() == Json.NodeType.ARRAY)) {
                                var treeiter = TreeIter();
                                store.append(out treeiter, parent);
                                store.set(treeiter, 0, _f(member), 1, "", -1);
                                walk_tree(content.get_object().get_member(member), store, treeiter);
                        }else{
                                var treeiter = TreeIter();
                                store.append(out treeiter, parent);
                                var member_object = content.get_object().get_member(member);

                                debug("%s is of type %s", member, member_object.get_value_type().name());

                                var member_node = content.get_object().get_member(member);
                                //message(member_node.get_value().strdup_contents());
                                string value = "";
                                debug(member_node.get_value_type().name());
                                debug(member_node.get_string());
                                switch(member_node.get_value_type()) {
                                case Type.STRING:
                                        value = member_node.get_string();
                                        break;
                                case Type.INT64:
                                        value = member_node.get_int().to_string();
                                        break;
                                case Type.BOOLEAN:
                                        value = member_node.get_boolean().to_string();
                                        break;
                                case Type.DOUBLE:
                                        value = member_node.get_double().to_string();
                                        break;
                                }
                                store.set(treeiter, 0, _f(member), 1, value, -1);

                        }
                }
        }else if (content.get_node_type() == Json.NodeType.ARRAY) {
                var treeiter = TreeIter();
                foreach (var item in content.get_array().get_elements()) {
                        debug("Type of item is %s", item.type_name());
                        if (item.get_node_type() == Json.NodeType.OBJECT) {
                                if (item.get_object().has_member("name")) {
                                        store.append(out treeiter, parent);
                                        store.set(treeiter, 0, _f(item.get_object().get_string_member("name")), 1, "", -1);
                                        //message(item.get_object().get_string_member("name"));
                                }else if (item.get_object().has_member("location")) {
                                        store.append(out treeiter, parent);
                                        store.set(treeiter, 0, _f(item.get_object().get_string_member("location")), 1, "", -1);
                                }else{
                                        store.append(out treeiter, parent);
                                        store.set(treeiter, 0, _f(item.get_object().get_members().first().data), 1, "", -1);
                                }
                                walk_tree(item, store, treeiter);
                        }else if (item.get_node_type() == Json.NodeType.ARRAY) {
                                walk_tree(item, store, treeiter);
                        }else if ((parent == null) && (item.get_node_type() != Json.NodeType.ARRAY) && (item.get_node_type()!= Json.NodeType.OBJECT)) {
                                store.append(out treeiter, parent);
                                debug("String: %s", item.get_value().strdup_contents());
                                store.set(treeiter, 0, _f(item.get_value().strdup_contents()), 1, item, -1);
                        }else if (item.get_node_type() == Json.NodeType.VALUE) {
                                //message(item.get_value().strdup_contents());
                                store.append(out treeiter, parent);
                                debug("String: %s", item.get_value().strdup_contents());
                                store.set(treeiter, 0, _f(item.get_value().strdup_contents()), 1, item.get_value().strdup_contents(), -1);
                        }
                }
        }else if (content.get_node_type() == Json.NodeType.VALUE) {
                TreeIter treeiter;
                //message(item.get_value().strdup_contents());
                store.append(out treeiter, parent);
                debug("String: %s", content.get_value().strdup_contents());
                store.set(treeiter, 0, _f(content.get_value().strdup_contents()), 1, content.get_value().strdup_contents(), -1);
        }

   }*/

	private string _f(string str){
		return Reformatter.format(str);
	}

	private void fill_stack(Json.Node node, ListBox? parent_box, string? requested_sub){
		var results_list = parent_box;
		if (parent_box == null) {
			results_list = new ListBox();
		}

		results_list.set_selection_mode(SelectionMode.NONE);

		if (node.get_node_type() == Json.NodeType.OBJECT) {
			foreach (var member in node.get_object().get_members()) {
				if (node.get_object().get_member(member).get_node_type() == Json.NodeType.VALUE) {
					debug("type is value");
					var value = get_value_contents(node.get_object().get_member(member));
					debug("value is %s", value);
					results_list = new ListBox();
					var results_row = new ListBoxRow();
					results_row.set_selectable(false);
					results_list.add(results_row);
					var label = new Label(value);
					label.set_line_wrap(true);
					label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR);
					results_row.add(label);

					stack.add_titled(results_list, member, _f(member));
				}else if (node.get_object().get_member(member).get_node_type() == Json.NodeType.ARRAY) {
					results_list = walk_array(node.get_object().get_array_member(member));

					stack.add_titled(results_list, member, _f(member));
				}
			}
		}else if (node.get_node_type() == Json.NodeType.ARRAY) {
			results_list = walk_array(node.get_array());
			stack.add_titled(results_list, requested_sub, _f(requested_sub));
		}
	}

	private Gtk.ListBox walk_array(Json.Array array){
		var box = new ListBox();
		foreach (var element in array.get_elements()) {
			if (element.get_node_type() == Json.NodeType.VALUE) {
				var row = new ListBoxRow();
				row.set_selectable(false);
				var label = new Label(get_value_contents(element));
				label.set_line_wrap(true);
				label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR);
				row.add(label);
				box.add(row);
			}else if (element.get_node_type() == Json.NodeType.OBJECT) {
				var row = new ListBoxRow();
				row.set_selectable(false);

				var object = element.get_object();
				var value = "";
				if (object.has_member("message"))
					value = get_value_contents(object.get_member("message"));
				else if (object.has_member("name"))
					value = get_value_contents(object.get_member("name"));
				else if (object.has_member("description"))
					value = get_value_contents(object.get_member("description"));
				else if (object.has_member("syndicate"))
					value = get_value_contents(object.get_member("syndicate"));
				else if (object.has_member("text"))
					value = get_value_contents(object.get_member("text"));
				else if (object.has_member("node"))
					value = get_value_contents(object.get_member("node"));
				else if (object.has_member("stats"))
					value = _f("stats");
				else{
					value = _f(element.get_object().get_member(element.get_object().get_members().nth_data(0)).get_string());
				}

				var expander = new Expander(value);
				var label = new Label(value); //No formatting needed because value is alreay formatted
				label.set_line_wrap(true);
				label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR);
				expander.set_label_widget(label);
				expander.add(walk_object(element.get_object()));
				row.add(expander);
				box.add(row);
			}
		}
		return box;
	}

	private Gtk.Box walk_object(Json.Object object){
		var stack = new Stack();
		var sidebar = new StackSwitcher();
		stack.set_hexpand(true);
		sidebar.set_stack(stack);
		sidebar.set_orientation(Orientation.VERTICAL);
		//sidebar.set_vexpand(true);
		foreach (var member in object.get_members()) {
			var box = new ListBox();
			box.set_selection_mode(SelectionMode.NONE);
			if (object.get_member(member).get_node_type() == Json.NodeType.VALUE) {
				var row = new ListBoxRow();
				var label = new Label(get_value_contents(object.get_member(member)));
				label.set_line_wrap(true);
				label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR);
				row.add(label);
				box.add(row);
			}else if (object.get_member(member).get_node_type() == Json.NodeType.ARRAY) {
				var expander = new Expander(null);
				var label = new Label(_f(member));
				label.set_line_wrap(true);
				label.set_line_wrap_mode(Pango.WrapMode.WORD_CHAR);
				expander.set_label_widget(label);
				var sub_box = walk_array(object.get_array_member(member));
				var row = new ListBoxRow();
				row.set_activatable(false);
				expander.add(sub_box);
				row.add(expander);
				box.add(row);
			}
			stack.add_titled(box, member, _f(member));
		}
		var top_box = new Box(Orientation.HORIZONTAL, 0);
		top_box.add(sidebar);
		top_box.add(stack);

		return top_box;
	}

	private string get_value_contents(Json.Node member_node){
		string value = "";
		switch(member_node.get_value_type()) {
		case Type.STRING:
			value = member_node.get_string();
			break;
		case Type.INT:
			value = member_node.get_int().to_string();
			break;
		case Type.INT64:
			value = member_node.get_int().to_string();
			break;
		case Type.BOOLEAN:
			value = member_node.get_boolean().to_string();
			break;
		case Type.DOUBLE:
			value = member_node.get_double().to_string();
			break;
		}
		return _f(value);
	}

	private Widget update_element(TargetName name){
		return Worldinfo.get_info_by_name(name);
	}

	private void add_to_stack(TargetName target_name, string info){
		stack.add_titled(update_element(target_name), info, _f(info));
	}

}
