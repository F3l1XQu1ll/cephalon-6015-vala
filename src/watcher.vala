using Gee;
using Worldinfo;
using Reformatter;

namespace Watcher {
	public enum TargetName {
		NEWS,
		EVENTS,
		SORTIE,
		SYNDICATES,
		FISSURES,
		INVASIONS,
		EARTH,
		CETUS,
		VALLIS,
		OUTPOSTS,
		ARBITRATION,
		NIGHTWAVE,
	}

	public enum Change {
		NEW_ITEM
	}

	private GLib.List <Target> targets;

	private void register_target(TargetName target_name,
	                             HashMap <string, Change> ? entrys){
		if (targets == null)
			targets = new GLib.List <Target>();

		if (entrys != null) {
			foreach (var key in entrys.keys) {
				var target = new Target(target_name, key, entrys.get(key));
				targets.append(target);
			}
		} else {
			var target = new Target(target_name, null, null);
			targets.append(target);
		}
	}

	public void start_watcher(){
		var thread_data = new BackgroundWatcher();

		try {
			Thread <void *> thread =
				new Thread <void *> .try ("background_watcher", thread_data.shedule);

				// thread.join();
		}
		catch (Error e)
		{
			warning("%s", e.message);
		}
	}

	private class Target
	{
		TargetName name;
		string sub_cathegory;
		Change change;
		Json.Node cache;

		public Target(TargetName name, string ? sub_cathegory, Change ? changes){
			this.name = name;

			if (sub_cathegory != null)
				this.sub_cathegory = sub_cathegory;

			if (changes != null)
				this.change = changes;
		}

		public TargetName get_name(){
			return name;
		}

		public bool has_sub_cathegory(){
			return sub_cathegory != null;
		}

		public string get_sub_cathegory(){
			return sub_cathegory;
		}

		public Change get_change(){
			return change;
		}

		public bool has_cache(){
			return cache != null;
		}

		public Json.Node get_cache(){
			return cache;
		}

		public void set_cache(Json.Node cache){
			this.cache = cache;
		}
	}

	private class BackgroundWatcher
	{
		public void * shedule(){
			while (true) {
				message("Thread up and running!");

				foreach (var target in targets) {
					var response = Worldinfo.get_direct_info(target.get_name());

					if (target.has_sub_cathegory()) {
						var sub_cathegory = target.get_sub_cathegory();
						var change = target.get_change();
						var node = response.get_object();
					} else {
						// no cathegory means no special change, so we expect a new item as
						// change …
						if (target.has_cache()) {
							if (!compare(target.get_cache().get_array(), response.get_array())) {       //
								                                                                    // TODO
								                                                                    // Fix
								                                                                    // comparison!
								message("A change was found!");
								target.set_cache(response);
							}
						} else
							target.set_cache(response);
					}
				}
				Thread.usleep(10000000);
			}
		} /* shedule */

		private bool compare(Json.Array list_1, Json.Array list_2){
			var first = list_1.get_elements().nth_data(0);
			var other_first = list_2.get_elements().nth_data(0);

			if ((first.get_node_type() == Json.NodeType.OBJECT) &&
			    first.get_object().has_member("id")) {
				if ((other_first.get_node_type() == Json.NodeType.OBJECT) &&
				    other_first.get_object().has_member("id")) {
					if (first.get_object().get_string_member("id") ==
					    other_first.get_object().get_string_member("id"))
						return true;
				}
			}
			return false;
		}
	}

	public class SelfUpdateingProperty : Object
	{
		public string initial_value;

// private int tick;
		private bool up;
		private int days = 0;
		private int hours = 0;
		private int minutes = 0;
		private int seconds = 0;
		private bool end_string;
		public SelfUpdateingProperty(string initial_value,
		                             bool up = true,
		                             bool end_string = true){
			this.initial_value = initial_value;
			var time = time_from_string(this.initial_value);

			this.seconds = time [3];
			this.minutes = time [2];
			this.hours = time [1];
			this.days = time [0];

			// this.tick = tick;
			this.up = up;
			this.end_string = end_string;
		}

		public string get_initial_value(){
			return initial_value;
		}

		public void shedule(){
			// while (true) {
			// message(initial_value);
			// message("days: %i, hours: %i, minutes: %i, seconds: %i", days, hours,
			// minutes, seconds);
			if (up) {
				seconds++;

				if (seconds == 60) {
					seconds = 0;
					minutes++;
				}

				if (minutes == 60) {
					minutes = 0;
					hours++;
				}

				if (hours == 24) {
					hours = 0;
					days++;
				}
			} else {
				seconds--;

				if (seconds == -1) {
					seconds = 59;
					minutes--;
				}

				if (minutes == -1) {
					minutes = 59;
					hours--;
				}

				if (hours == -1) {
					hours = 23;
					days--;
				}
			}

			// message("days: %i, hours: %i, minutes: %i, seconds: %i", days, hours,
			// minutes, seconds);
			if (up)
				this.initial_value = string_from_time({ days, hours, minutes,
				                                        seconds },
				                                      "ago.");
			else if (end_string)
				this.initial_value = string_from_time({ days, hours,
				                                        minutes,
				                                        seconds },
				                                      "to go.");
			else
				this.initial_value = string_from_time({ days, hours, minutes, seconds },
				                                      "");

			// message(_initial_value);
			// Thread.usleep(1000000);
			// }
		} /* shedule */

		private int[] time_from_string(string input){
			/*var parts = input.split("d");
			 * var days = parts[0];
			 * parts = parts[1].split("h");
			 * //message(parts.length.to_string());
			 * var hours = parts[0];
			 * parts = parts[1].split("m");
			 * //message(parts.length.to_string());
			 * var minutes = parts[0];
			 * parts = parts[1].split("s");
			 * //message(parts.length.to_string());
			 * var seconds = parts[0];*/

			var time = new DateTime.from_iso8601(input, null);
			var difference = time.difference(new DateTime.now_utc()).abs();
			var days = (difference / TimeSpan.DAY);

			// message(days.to_string());
			var hours = ((difference % TimeSpan.DAY) / TimeSpan.HOUR);

			// message(hours.to_string());
			var minutes = ((difference % TimeSpan.HOUR) / TimeSpan.MINUTE);

			// message(minutes.to_string());
			var seconds = ((difference % TimeSpan.MINUTE) / TimeSpan.SECOND);

			return { int.parse(days.to_string()), int.parse(hours.to_string()), int.parse(
					 minutes.to_string()), int.parse(seconds.to_string()) };
		}

		private string string_from_time(int[] input, string end){
			var days = input [0];
			var hours = input [1];
			var minutes = input [2];
			var seconds = input [3];

			var text = days.to_string() + "d " + hours.to_string() + "h " +
			           minutes.to_string() + "m " + seconds.to_string() + "s " + end;

			// var text = days.to_string() + "d " + hours.to_string() + "h " +
			// minutes.to_string() + "m ago";
			// message(text);
			return text;
		}
	}

	public class Worker {
		public bool run = true;
		public int delay_seconds = 0;

		public Worker(int delay_seconds){
			this.delay_seconds = delay_seconds;
		}

		public virtual void shedule(){
			while (run) {
				message("Thread is running");
				Thread.usleep(delay_seconds * 1000000);
			}
		}

		public void stop(){
			run = false;
		}
	}

	public class UpdateWorker : Worker {
		public UpdateWorker(int delay_seconds){
			base(delay_seconds);
		}

		public override void shedule(){
			while (run) {
				update_event();
				Thread.usleep(delay_seconds * 1000000);
			}
		}

		public signal void update_event();
	}

	public class MethodWorker : Worker {

		public delegate Gtk.Widget? Delegate ();
		private Delegate exec;

		public MethodWorker(int delay_seconds, Delegate exec){
			base(delay_seconds);
			this.exec = exec;
		}

		public override void shedule(){
			while (run) {
				var widget = exec();
				if (widget != null)
					got_element(widget);
				Thread.usleep(delay_seconds * 1000000);
			}
		}

		public signal void got_element(Gtk.Widget widget);
	}
}
