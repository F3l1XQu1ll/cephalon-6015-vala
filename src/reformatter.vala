namespace Reformatter {
	public string format(string input){
		var target = input;
		if (target != null && target != "") {
			fix_case(target, out target);
			fix_spaces(target, out target);
			fix_dots(target, out target);
			remove_quotations(target, out target);
			fix_new_line(target, out target);
		}
		return target;
	}

	private void fix_case(string input, out string output){
		var to_up_letter = input.get(0);
		var to_keep_text = input.slice(1, input.length);
		output = to_up_letter.toupper().to_string() + to_keep_text;
	}

	private void fix_spaces(string input, out string output){
		var no_first_letter = input.slice(1, input.length);
		var old_chars = no_first_letter.to_utf8();
		var to_fix_positions = new List<char>();
		to_fix_positions.append(old_chars[0]);
		for (var i = 1; i < old_chars.length; i++) {
			if (old_chars[i].isupper() && old_chars[i - 1].islower()) {
				to_fix_positions.append(' ');
				to_fix_positions.append(old_chars[i].toupper());
			}else{
				to_fix_positions.append(old_chars[i]);
			}
		}

		output = "";
		foreach (var character in to_fix_positions) {
			output = output + character.to_string();
		}
		output = input.get(0).to_string() + output;
	}

	private void fix_dots(string input, out string output){
		var old_chars = input.to_utf8();
		var to_fix_positions = new List<char>();
		for (var i = 0; i < old_chars.length; i++) {
			if (old_chars[i] == '.' && old_chars[i + 1] != ' ') {
				to_fix_positions.append(' ');
				to_fix_positions.append(old_chars[i].toupper());
			}else{
				to_fix_positions.append(old_chars[i]);
			}
		}

		output = "";
		var builder = new StringBuilder();
		foreach (var character in to_fix_positions) {
			//output = output + character.to_string();
			builder.append_c(character);
		}
		output = builder.str;

	}

	private void remove_quotations(string input, out string output){
		output = input;
		//message("has prefix? %s", input.has_prefix("\"").to_string());
		if (input.has_prefix("\"")) {
			output = output.substring(1, input.length - 1);
		}

		if (input.has_suffix("\"")) {
			output = output.substring(0, input.length - 2);
		}
	}

	private void fix_new_line(string input, out string output){
		output = input;
		if (input.contains("\\n")) {
			output = input.replace("\\n", "\n");
		}
	}

	public int[] time_from_string(string input){
		message(input);
		var parts = input.split("d");
		var days = parts[0];
		parts = parts[1].split("h");
		//message(parts.length.to_string());
		var hours = parts[0];
		parts = parts[1].split("m");
		//message(parts.length.to_string());
		var minutes = parts[0];
		parts = parts[1].split("s");
		//message(parts.length.to_string());
		var seconds = parts[0];

		return {int.parse(days), int.parse(hours), int.parse(minutes), int.parse(seconds)};
	}

	public string string_from_time(int[] input){
		var days = input[0];
		var hours = input[1];
		var minutes = input[2];
		var seconds = input[3];

		var text = days.to_string() + "d " + hours.to_string() + "h " + minutes.to_string() + "m " + seconds.to_string() + "s ago.";
		//message(text);
		return text;
	}

}