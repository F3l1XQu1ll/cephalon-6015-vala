using Gtk;
using Cache;
using Gee;
using Reformatter;
using Values;

[GtkTemplate(ui = "/ui/settings.ui")]
public class AppSettingsWindow : Gtk.Window {
	[GtkChild]
	private Gtk.Viewport viewport;
	[GtkChild]
	private Gtk.StackSidebar sidebar;



//private HashMap<Gtk.CheckButton, string> button_map = new HashMap<Gtk.CheckButton, string>();

	public AppSettingsWindow(){
		this.set_keep_above(true);

		//var notebook = new Gtk.Notebook();
		var stack = new Gtk.Stack();
		//notebook.set_scrollable(true);
		//notebook.set_tab_pos(Gtk.PositionType.LEFT);
		//notebook.popup_enable();
		//viewport.add(notebook);
		viewport.add(stack);

		/*
		   global config
		 */
		setup_globals(stack);


		foreach (var category in Cache.Categories) {
			var blacklist = load_blacklist(category);
			var listbox = new ListBox();
			var scrolledWindow = new ScrolledWindow(null, null);
			scrolledWindow.add(listbox);
			stack.add_titled(scrolledWindow, category, format(category));
			ArrayList<string> names = get_category_attributes(category);
			foreach (var key in names) {
				var listBoxRow = new ListBoxRow();
				var grid = new Box(Gtk.Orientation.HORIZONTAL, 0);
				listBoxRow.add(grid);

				var key_label = new Label(format(key));
				var key_toggle = new Switch();
				if (blacklist.contains(key)) {
					key_toggle.set_active(false);
				}else{
					key_toggle.set_active(true);
				}
				//key_check.connect("signal::toggled", on_toggle, key_check, key, category);
				//key_toggle.activate.connect(e => {on_toggle(e, category, key);});
				key_toggle.notify["active"].connect(() => {on_toggle(key_toggle, category, key);});
				//key_check.set_property("key", key);

				//button_map.set(key_check, key);

				grid.pack_start(key_label, false, false, 0);
				grid.pack_end(key_toggle, false, true, 0);
				listbox.add(listBoxRow);
			}
		}
		sidebar.set_stack(stack);
		this.show_all();
	}

	private void setup_globals(Stack stack){
		var globals_listbox = new ListBox();
		var globals_scrolledWindow = new ScrolledWindow(null, null);

		globals_scrolledWindow.add(globals_listbox);
		stack.add_titled(globals_scrolledWindow, "Globals", "Globals");

		add_row_by_name(globals_listbox, SUPPRESS_UNIQUE_NAMES, "suppress uniqueNames");
		add_row_by_name(globals_listbox, SUPPRESS_NAMES, "suppress names");
	}

	private void add_row_by_name(ListBox globals_listbox, string setting, string description){
		var suppress_unique_names_listBoxRow = new ListBoxRow();
		var suppress_unique_names_hbox = new Box(Gtk.Orientation.HORIZONTAL, 0);
		suppress_unique_names_listBoxRow.add(suppress_unique_names_hbox);
		var settings = new GLib.Settings("com.archyt.cephalon-6015");
		var suppress_uniqueNames = settings.get_boolean(setting);

		var suppress_uniqueNames_label = new Label(format(description));
		var suppress_uniqueNames_toggle = new Switch();
		if (suppress_uniqueNames) {
			suppress_uniqueNames_toggle.set_active(true);
		}else{
			suppress_uniqueNames_toggle.set_active(false);
		}

		suppress_uniqueNames_toggle.notify["active"].connect(() => {
			message("setting %s to %s", setting, suppress_uniqueNames_toggle.get_active().to_string());
			settings.set_boolean(setting, suppress_uniqueNames_toggle.get_active());
		});

		suppress_unique_names_hbox.pack_start(suppress_uniqueNames_label, false, false, 0);
		suppress_unique_names_hbox.pack_end(suppress_uniqueNames_toggle, false, true, 0);
		globals_listbox.add(suppress_unique_names_listBoxRow);
	}

	[GtkCallback]
	private void on_button_quit_clicked(Button button){
		this.destroy();
	}

	private void on_toggle(Switch button, string category, string key){

		var blacklist = load_blacklist(category);
		message("Does blacklist even exist?! %s", blacklist.to_array().length.to_string());
		//message("Blacklist contains key? %s; Is button active? %s",
		//        blacklist.contains(key).to_string(), button.get_active().to_string());
		/*if (!blacklist.is_empty) {
		        if (blacklist.contains(key) && (button.get_active() == true)) {
		                blacklist.remove(key);
		        }else if ((blacklist.contains(key) == false) && (button.get_active() == false)) {
		                blacklist.add(key);
		        }
		   }else{
		   } */
		message(button.get_active().to_string());

		if (!button.get_active()) {
			if (!blacklist.is_empty) {
				if (!blacklist.contains(key)) {
					blacklist.add(key);
				}
			}else{
				blacklist.add(key);
			}
		}else{
			if (!blacklist.is_empty) {
				if (blacklist.contains(key)) {
					blacklist.remove(key);
				}
			}
		}
		store_blacklist(blacklist, category);
	}
}